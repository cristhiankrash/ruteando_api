from django.http import HttpResponse, JsonResponse

from datetime import datetime

def view_def(request):
    now = datetime.now()
    return HttpResponse(f'Hola Cristhian la hora es {str(now)}')

def holaJsonResponse(request):
    return JsonResponse({"hola":"mundo"})