from rest_framework import serializers
from .models import CityModel
from .models import RouterModel
from .models import VoyagerModel
from .models import ProfileModel
from .models import VehicleModel
from .models import TravelModel


class CitySerializer (serializers.ModelSerializer):
    class Meta:
        model = CityModel
        fields = '__all__'


class RouterSerializer (serializers.ModelSerializer):
    class Meta:
        model = RouterModel
        fields = '__all__'


class VoyagerSerializer (serializers.ModelSerializer):
    class Meta:
        model = VoyagerModel
        fields = '__all__'


class ProfileSerializer (serializers.ModelSerializer):
    class Meta:
        model = ProfileModel
        fields = '__all__'


class VehicleSerializer (serializers.ModelSerializer):
    class Meta:
        model = VehicleModel
        fields = '__all__'


class TravelSerializer (serializers.ModelSerializer):
    class Meta:
        model = TravelModel
        fields = '__all__'
