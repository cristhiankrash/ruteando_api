from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from django.http import HttpResponse, JsonResponse
from datetime import datetime
from ..models import RouterModel
from ..serializers import RouterSerializer


class RouterView(APIView):
    def get(self, request):
        router = RouterModel.objects.all()
        serializer = RouterSerializer(router, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        print(request.data)
        router = request.data
        serializer = RouterSerializer(data=router)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request):
        id = request.query_params['id']
        router_new = request.data
        if id and router_new:
            router = RouterModel.objects.filter(id=id).first()
            if router:
                serializer = RouterSerializer(
                    instance=router, data=router_new, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response({"message": "no Foud"}, status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request):
        id = request.query_params['id']
        router = RouterModel.objects.filter(id=id)
        if router:
            router.delete()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @classmethod
    def get_extra_actions(cls):
        return []

