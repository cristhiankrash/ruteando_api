from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from django.http import HttpResponse, JsonResponse
from datetime import datetime
from ..models import VehicleModel
from ..serializers import VehicleSerializer


class VehicleView(APIView):
    def get(self, request):
        vehicle = VehicleModel.objects.all()
        serializer = vehicleSerializer(vehicle, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        print(request.data)
        vehicle = request.data
        serializer = VehicleSerializer(data=vehicle)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request):
        id = request.query_params['id']
        vehicle_new = request.data
        if id and vehicle_new:
            vehicle = VehicleModel.objects.filter(id=id).first()
            if vehicle:
                serializer = VehicleSerializer(
                    instance=vehicle, data=vehicle_new, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response({"message": "no Foud"}, status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request):
        id = request.query_params['id']
        vehicle = VehicleModel.objects.filter(id=id)
        if vehicle:
            vehicle.delete()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @classmethod
    def get_extra_actions(cls):
        return []

