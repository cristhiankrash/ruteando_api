from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from datetime import datetime
from ..models import CityModel

from ..serializers import CitySerializer      # add this


class CityView(APIView):
    def get(self, request):
        cities = CityModel.objects.all()
        serializer = CitySerializer(cities, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        print(request.data)
        city = request.data
        serializer = CitySerializer(data=city)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request):
        id = request.query_params['id']
        city_new = request.data
        if id and city_new:
            city = CityModel.objects.filter(id=id).first()
            if city:
                serializer = CitySerializer(
                    instance=city, data=city_new, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response({"message": "no Foud"}, status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request):
        id = request.query_params['id']
        city = CityModel.objects.filter(id=id)
        if city:
            city.delete()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    # if 'django' not in value.lower():
    #         raise serializers.ValidationError("Blog post is not about Django")

    @classmethod
    def get_extra_actions(cls):
        return []
    # def get(self, request):
    #     cities_filter = self.filterObj(dict(request.GET))
    #     data = list(cities_filter.values("id","name","state","date_create","date_update"))
    #     return JsonResponse(data, safe=False) # safe=Fase es por uqe no siempre devuelce un json si no un array

    # def post(self,request, *args, **kwargs):
    #     city = request.data
    #     res = CityModel.objects.i

    # def filterObj(self, filters):
    #     cities_filter = {}
    #     print("filter")
    #     print(filter)
    #     if len(filters.values()) > 0:
    #         for city in filters:
    #             print(city)
    #             # if len(cities_filter) == 0:
    #             #     if filters.get('id'): cities_filter = CityModel.objects.filter(id=filters['id'][0])
    #             #     if filters.get('name'): cities_filter = CityModel.objects.filter(name=filters['name'][0])
    #             #     if filters.get('state'): cities_filter = CityModel.objects.filter(state=filters['state'][0])
    #             #     if filters.get('date_create'): cities_filter = CityModel.objects.filter(date_create=filters['date_create'][0])
    #             #     if filters.get('date_update'): cities_filter = CityModel.objects.filter(date_update=filters['date_update'][0])
    #             #     return cities_filter
    #             # else:
    #             #     if filters.get('id'): cities_filter = cities_filter.filter(id=filters['id'][0])
    #             #     if filters.get('name'): cities_filter = CityModel.objects.filter(name=filters['name'][0])
    #             #     if filters.get('state'): cities_filter = CityModel.objects.filter(state=filters['state'][0])
    #             #     if filters.get('date_create'): cities_filter = CityModel.objects.filter(date_create=filters['date_create'][0])
    #             #     if filters.get('date_update'): cities_filter = CityModel.objects.filter(date_update=filters['date_update'][0])
    #             #     return cities_filter
    #         return []
    #     else:
    #         print("entro a all")
    #         cities_filter = CityModel.objects.all()
    #         return cities_filter
    # def getDetail(self, request, param):
    #     print(param)
    #     return JsonResponse({"hola":"chao"})
        # city = self.cityModel.objects.filter(id=)
