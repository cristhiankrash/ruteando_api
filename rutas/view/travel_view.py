from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from django.http import HttpResponse, JsonResponse
from datetime import datetime
from ..models import TravelModel
from ..serializers import TravelSerializer


class TravelView(APIView):
    def get(self, request):
        travel = TravelModel.objects.all()
        serializer = TravelSerializer(travel, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        print(request.data)
        travel = request.data
        serializer = TravelSerializer(data=travel)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request):
        id = request.query_params['id']
        travel_new = request.data
        if id and travel_new:
            travel = TravelModel.objects.filter(id=id).first()
            if travel:
                serializer = TravelSerializer(
                    instance=travel, data=travel_new, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response({"message": "no Foud"}, status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request):
        id = request.query_params['id']
        travel = TravelModel.objects.filter(id=id)
        if travel:
            travel.delete()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @classmethod
    def get_extra_actions(cls):
        return []

