from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from django.http import HttpResponse, JsonResponse
from datetime import datetime
from ..models import VoyagerModel
from ..serializers import VoyagerSerializer


class VoyagerView(APIView):
    def get(self, request):
        voyager = VoyagerModel.objects.all()
        serializer = VoyagerSerializer(voyager, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        print(request.data)
        voyager = request.data
        serializer = VoyagerSerializer(data=voyager)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request):
        id = request.query_params['id']
        voyager_new = request.data
        if id and voyager_new:
            voyager = VoyagerModel.objects.filter(id=id).first()
            if voyager:
                serializer = VoyagerSerializer(
                    instance=voyager, data=voyager_new, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response({"message": "no Foud"}, status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request):
        id = request.query_params['id']
        voyager = VoyagerModel.objects.filter(id=id)
        if voyager:
            voyager.delete()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @classmethod
    def get_extra_actions(cls):
        return []

