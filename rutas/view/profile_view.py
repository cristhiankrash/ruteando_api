from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from django.http import HttpResponse, JsonResponse
from datetime import datetime
from ..models import ProfileModel
from ..serializers import ProfileSerializer


class ProfileView(APIView):
    def get(self, request):
        profile = ProfileModel.objects.all()
        serializer = ProfileSerializer(profile, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        print(request.data)
        profile = request.data
        serializer = ProfileSerializer(data=profile)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request):
        id = request.query_params['id']
        profile_new = request.data
        if id and profile_new:
            profile = ProfileModel.objects.filter(id=id).first()
            if profile:
                serializer = ProfileSerializer(
                    instance=profile, data=profile_new, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response({"message": "no Foud"}, status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request):
        id = request.query_params['id']
        profile = ProfileModel.objects.filter(id=id)
        if profile:
            profile.delete()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @classmethod
    def get_extra_actions(cls):
        return []

