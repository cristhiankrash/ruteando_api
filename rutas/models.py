from django.contrib.auth.models import User
from django.db import models
import uuid


class CityModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    state = models.CharField(max_length=128)

    date_create = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(null=True, auto_now=True)

    def __str__(self):
        return f'{self.name} - {self.state}'

    class Meta:
        db_table = 'rutas_citiesmodel'


class ProfileModel(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    last_name = models.CharField(max_length=128)
    phone_number = models.CharField(max_length=18)
    type_identification = models.CharField(max_length=32)
    identification_number = models.CharField(max_length=32)
    city_id = models.ForeignKey(
        CityModel, blank=True, on_delete=models.SET_NULL, null=True)
    address = models.CharField(max_length=128)
    bithdate = models.DateField(blank=True, null=True)
    photo_perfil = models.ImageField(
        upload_to='users/perfil',
        blank=True,
        null=True
    )

    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user}'

    class Meta:
        db_table = 'rutas_profilesmodel'


class VehicleModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    color = models.CharField(max_length=64)
    licence_plate = models.CharField(max_length=16)
    user_id = models.ForeignKey(
        ProfileModel, blank=True, null=True, on_delete=models.SET_NULL)
    model = models.CharField(max_length=64)
    mark = models.CharField(max_length=64)

    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)
    # legacity_id =

    def __str__(self):
        return f'{self.mark} - {self.model}'

    class Meta:
        db_table = 'rutas_vehiclemodel'


class TravelModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    diver_id = models.ForeignKey(
        ProfileModel, blank=False, null=True, on_delete=models.SET_NULL)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(blank=True, null=True)
    start_point = models.CharField(max_length=512)
    end_point = models.CharField(max_length=512)
    capacity_person = models.IntegerField(blank=True)  # maybe
    vehicle_id = models.ForeignKey(
        VehicleModel, blank=False, null=True, on_delete=models.SET_NULL)
    distance = models.CharField(max_length=64)
    commentary = models.CharField(max_length=512)
    is_active = models.BooleanField(default=False)

    date_create = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'rutas_travelmodel'


class RouterModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    origin = models.ForeignKey(
        CityModel, blank=False, null=True, on_delete=models.SET_NULL, related_name='origin_city')
    destinity = models.ForeignKey(
        CityModel, blank=False, null=True, on_delete=models.SET_NULL, related_name='destinity_city')
    travel_id = models.ForeignKey(
        TravelModel, blank=False, null=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=False)

    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'from: {self.self.origin} to: {self.self.destinity}'

    class Meta:
        db_table = 'rutas_routermodel'


class VoyagerModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        ProfileModel, blank=False, null=True, on_delete=models.SET_NULL)
    route_id = models.ForeignKey(
        RouterModel, blank=False, null=True,on_delete=models.SET_NULL)

    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'rutas.voyagermodel'
