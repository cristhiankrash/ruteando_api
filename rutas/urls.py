from django.urls import path, include
from .view.city_view import CityView
from .view.voyager_view import VoyagerView
from .view.vehicle_view import VehicleView
from .view.travel_view import TravelView
from .view.router_view import RouterView
from .view.profile_view import ProfileView
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'posts', CityView, 'Crud')

urlpatterns = [
    path("city", CityView.as_view()),
    path('voyager', VoyagerView.as_view()),
    path("vehicle", VehicleView.as_view()),
    path('travel', TravelView.as_view()),
    path("router", RouterView.as_view()),
    path("profile", ProfileView.as_view())
    # path("city<uuid:id>", CityView.getDetail, name="city_detail"),
]